package foxminded.products.Dagger.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import java.util.Objects;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import foxminded.products.MyApp;
import foxminded.products.RoomDB.AppDatabase;
import foxminded.products.RoomDB.FavoriteDao;

/**
 * Application Module.
 */

@Module(includes = {NetworkModule.class})
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(MyApp application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room.databaseBuilder(Objects.requireNonNull(context),
                AppDatabase.class, "favoriteProducts.db").build();
    }

    @Singleton
    @Provides
    public FavoriteDao provideFavoriteDao(AppDatabase appDatabase){
        return appDatabase.getFavoriteDao();
    }
}
