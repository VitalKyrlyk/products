package foxminded.products.Dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import foxminded.products.Retrofit.ApiInterface;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * NNetwork module.
 */
@Module
public class NetworkModule {

    private static final String BASE_URL = "https://openapi.etsy.com/v2/";

    @Provides
    @Singleton
    OkHttpClient provideOkHttp() {
        return new OkHttpClient.Builder().build();
    }

//TODO: uncomment when start using retrofit 2

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build();
    }


    @Provides
    @Singleton
    ApiInterface provideApi(Retrofit retrofit) {
        return retrofit.create(ApiInterface.class);
    }

}