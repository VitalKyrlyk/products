package foxminded.products.Dagger;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import foxminded.products.Fragments.FvoriteFragment.FavoriteFragment;
import foxminded.products.Fragments.SearchFragment.SearchFragment;

@Module
public abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract FavoriteFragment bindFavoriteFragment();

    @ContributesAndroidInjector
    abstract SearchFragment bindSearchFragment();

}
