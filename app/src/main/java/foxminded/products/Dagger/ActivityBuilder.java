package foxminded.products.Dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import foxminded.products.Activity.DetailedActivity.DetailedActivity;
import foxminded.products.Activity.MainActivity;

/**
 * Class declare to which activities dependency are injected.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector
    abstract DetailedActivity bindDetailedActivity();

}
