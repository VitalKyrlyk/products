package foxminded.products.Dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import foxminded.products.Dagger.ActivityBuilder;
import foxminded.products.Dagger.FragmentBuilder;
import foxminded.products.Dagger.module.AppModule;
import foxminded.products.Dagger.module.NetworkModule;
import foxminded.products.MyApp;

/**
 * Application Component.
 */

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        ActivityBuilder.class,
        FragmentBuilder.class})

public interface AppComponent extends AndroidInjector<MyApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<MyApp> {
    }
}