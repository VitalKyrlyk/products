package foxminded.products.utils;

public interface OnNextPageListener {
    boolean onLoadMore();
}
