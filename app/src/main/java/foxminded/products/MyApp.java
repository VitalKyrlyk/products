package foxminded.products;


import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import foxminded.products.Dagger.components.DaggerAppComponent;

public class MyApp extends DaggerApplication{

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }
}
