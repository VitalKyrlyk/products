package foxminded.products.Activity.DetailedActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import foxminded.products.Activity.DetailedActivity.Presenter.DetailedActivityPresenter;
import foxminded.products.Activity.DetailedActivity.View.DetailedActivityView;
import foxminded.products.R;
import foxminded.products.RoomDB.FavoriteDao;

public class DetailedActivity extends DaggerAppCompatActivity implements DetailedActivityView {

    String title;
    String image;
    String description;
    String price;
    int listing_id;

    DetailedActivityPresenter activityPresenter;

    @Inject
    protected FavoriteDao favoriteDao;

    @BindView(R.id.ibFavoriteDetailed)
    ImageButton ibFavoriteDetailed;

    @OnClick(R.id.ibFavoriteDetailed)
    void setIbFavoriteDetailedClicked(){
        if (ibFavoriteDetailed.isSelected()){
            activityPresenter.deleteFromFavorite(listing_id);
            ibFavoriteDetailed.setSelected(false);
            ibFavoriteDetailed.setImageResource(R.drawable.btn_star_big_off);
            } else {
            activityPresenter.addToFavorite(title, image, description, price, listing_id);
        }
    }

    @BindView(R.id.ivIconDetailed)
    ImageView ivIconDetailed;

    @BindView(R.id.tvDescriptionDetailed)
    TextView tvDescriptionDetailed;

    @BindView(R.id.tvPriceDetailed)
    TextView tvPriceDetailed;

    @BindView(R.id.tvTitleDetailed)
    TextView tvTitleDetailed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        ButterKnife.bind(this);

        activityPresenter = DetailedActivityPresenter.getInstance(favoriteDao);
        activityPresenter.getView(this);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        ArrayList<String> list = bundle.getStringArrayList("list");
        assert list != null;
        title = list.get(0);
        description = list.get(1);
        price = list.get(2);
        image = list.get(3);
        listing_id = Integer.parseInt(list.get(4));

        tvTitleDetailed.setText(title);
        tvDescriptionDetailed.setText(description);
        tvPriceDetailed.setText(price);
        if (image != null){
            Glide.with(this)
                    .load(image)
                    .into(ivIconDetailed);
        }

        activityPresenter.getFavoriteList(this, listing_id);
    }

    @Override
    public void checkInFavorite() {
        ibFavoriteDetailed.setSelected(true);
        ibFavoriteDetailed.setImageResource(R.drawable.btn_star_big_on);
    }
}
