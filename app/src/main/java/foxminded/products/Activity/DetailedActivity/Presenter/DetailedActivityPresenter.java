package foxminded.products.Activity.DetailedActivity.Presenter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import foxminded.products.Activity.DetailedActivity.View.DetailedActivityView;
import foxminded.products.RoomDB.FavoriteDao;
import foxminded.products.RoomDB.FavoriteProductsData;

public class DetailedActivityPresenter {

    private static DetailedActivityPresenter instance;
    private DetailedActivityView activityView;
    private FavoriteDao favoriteDao;

    private DetailedActivityPresenter(FavoriteDao favoriteDao) {
        this.favoriteDao = favoriteDao;
    }

    public static synchronized DetailedActivityPresenter getInstance(FavoriteDao favoriteDao){
        if (instance == null){
            instance = new DetailedActivityPresenter(favoriteDao);
        }
        return instance;
    }

    public void getView(DetailedActivityView detailedActivityView){
        this.activityView = detailedActivityView;
    }

    public void addToFavorite(String title, String image, String description,
                              String price, int listing_id){
        final FavoriteProductsData data = new FavoriteProductsData();
        data.setTitle(title);
        data.setDescription(description);
        data.setPrice(price);
        data.setImage(image);
        data.setListing_id(listing_id);
        AsyncTask.execute(() -> favoriteDao.insert(data));
    }

    public void deleteFromFavorite(int listing_id){
        AsyncTask.execute(() -> favoriteDao.delete(listing_id));
    }

    public void getFavoriteList(LifecycleOwner owner, int listing_id){
        LiveData<List<FavoriteProductsData>> liveData = favoriteDao.getAllFavoriteProducts();
        liveData.observe(owner, favoriteProductsData -> {
            assert favoriteProductsData != null;
            for (int i = 0; i < favoriteProductsData.size(); i++){
                int id = favoriteProductsData.get(i).getListing_id();
                if (listing_id == id){
                    activityView.checkInFavorite();
                }
            }
        });

    }

}
