package foxminded.products.Activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.ViewPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import butterknife.BindView;
import butterknife.ButterKnife;
import foxminded.products.Fragments.FvoriteFragment.FavoriteFragment;
import foxminded.products.Fragments.SearchFragment.SearchFragment;
import foxminded.products.R;

public class MainActivity extends AppCompatActivity {

    FragmentPagerItemAdapter fragmentPagerItemAdapter;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.viewPagerTabLayout)
    SmartTabLayout smartTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Search", SearchFragment.class)
                .add("Favorite", FavoriteFragment.class)
                .create());

        viewPager.setAdapter(fragmentPagerItemAdapter);
        smartTabLayout.setViewPager(viewPager);
    }
}
