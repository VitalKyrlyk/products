package foxminded.products.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {FavoriteProductsData.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static String DATABASE_NAME = "favoriteProducts";

    public abstract FavoriteDao getFavoriteDao();
}
