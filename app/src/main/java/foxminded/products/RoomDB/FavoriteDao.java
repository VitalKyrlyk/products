package foxminded.products.RoomDB;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface FavoriteDao {

    @Query("SELECT * FROM favoriteproductsdata ORDER BY id DESC")
    LiveData<List<FavoriteProductsData>> getAllFavoriteProducts();

    @Query("DELETE FROM favoriteproductsdata WHERE listing_id = :listing_id")
    void delete(int listing_id);

    @Insert
    void insertAll(FavoriteProductsData... favoriteProductsData);

    @Insert
    void insert(FavoriteProductsData favoriteProductsData);

    @Insert
    void delete(FavoriteProductsData favoriteProductsData);
}
