package foxminded.products.Fragments.FvoriteFragment.FavoriteFragmentView;

import java.util.List;

import foxminded.products.RoomDB.FavoriteProductsData;

public interface FavoriteFragmentView {

    void getFavoriteList(List<FavoriteProductsData> dataList);

}
