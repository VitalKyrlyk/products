package foxminded.products.Fragments.FvoriteFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import foxminded.products.Fragments.FvoriteFragment.FavoriteFragmentPresenter.FavoriteFragmentPresenter;
import foxminded.products.Fragments.FvoriteFragment.FavoriteFragmentView.FavoriteFragmentView;
import foxminded.products.Fragments.FvoriteFragment.rvAdapter.FavoriteFragmentAdapter;
import foxminded.products.R;
import foxminded.products.RoomDB.FavoriteDao;
import foxminded.products.RoomDB.FavoriteProductsData;

public class FavoriteFragment extends DaggerFragment implements FavoriteFragmentView {

    View view;
    FavoriteFragmentAdapter adapter;
    FavoriteFragmentPresenter presenter;

    @Inject
    protected FavoriteDao favoriteDao;

    @BindView(R.id.rvFavoriteList)
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite, null);
        ButterKnife.bind(this, view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        adapter = new FavoriteFragmentAdapter(favoriteDao, getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        presenter = FavoriteFragmentPresenter.getInstance(favoriteDao);
        presenter.getView(this);
        presenter.getFavoriteList(this);

        return view;
    }

    @Override
    public void getFavoriteList(List<FavoriteProductsData> dataList) {
        adapter.setData(dataList);
    }
}
