package foxminded.products.Fragments.FvoriteFragment.rvAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import foxminded.products.Activity.DetailedActivity.DetailedActivity;
import foxminded.products.R;
import foxminded.products.RoomDB.FavoriteDao;
import foxminded.products.RoomDB.FavoriteProductsData;

public class FavoriteFragmentAdapter extends RecyclerView.Adapter<FavoriteFragmentAdapter.ViewHolder> {

    private List<FavoriteProductsData> data;
    private FavoriteDao favoriteDao;
    private Context context;

    public void setData(List<FavoriteProductsData> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public FavoriteFragmentAdapter(FavoriteDao favoriteDao, Context context) {
        this.favoriteDao = favoriteDao;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
        return new FavoriteFragmentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvTitleFavorite.setText(data.get(position).getTitle());
        String image = data.get(position).getImage();
        if (image != null){
            Glide.with(context)
                    .load(image)
                    .into(holder.ivIconFavorite);
        }

        holder.ibFavoriteFavorite.setOnClickListener(view ->
                AsyncTask.execute(() -> favoriteDao.delete(data.get(position).getListing_id())));

        if (data != null) {
            holder.view.setOnClickListener(view -> {
                ArrayList<String> list = new ArrayList<>();
                list.add(0, data.get(position).getTitle());
                list.add(1, data.get(position).getDescription());
                list.add(2, data.get(position).getPrice());
                list.add(3, data.get(position).getImage());
                list.add(4, String.valueOf(data.get(position).getListing_id()));

                Intent intent = new Intent(context, DetailedActivity.class);
                intent.putStringArrayListExtra("list", list);
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (data == null){
            return 0;
        }
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitleFavorite;
        ImageButton ibFavoriteFavorite;
        ImageView ivIconFavorite;
        View view;

        ViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            tvTitleFavorite = itemView.findViewById(R.id.tvTitleFavorite);
            ibFavoriteFavorite = itemView.findViewById(R.id.ibFavoriteFavorite);
            ivIconFavorite = itemView.findViewById(R.id.ivIconFavorite);
        }
    }
}
