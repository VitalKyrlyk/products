package foxminded.products.Fragments.FvoriteFragment.FavoriteFragmentPresenter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;

import java.util.List;

import foxminded.products.Fragments.FvoriteFragment.FavoriteFragmentView.FavoriteFragmentView;
import foxminded.products.RoomDB.FavoriteDao;
import foxminded.products.RoomDB.FavoriteProductsData;

public class FavoriteFragmentPresenter {

    private static FavoriteFragmentPresenter instance;
    private FavoriteFragmentView fragmentView;
    private FavoriteDao favoriteDao;

    private FavoriteFragmentPresenter(FavoriteDao favoriteDao) {
        this.favoriteDao = favoriteDao;
    }

    public static synchronized FavoriteFragmentPresenter getInstance(FavoriteDao favoriteDao) {
        if (instance == null) {
            instance = new FavoriteFragmentPresenter(favoriteDao);
        }
        return instance;
    }

    public void getView(FavoriteFragmentView fragmentView) {
        this.fragmentView = fragmentView;
    }

    public void getFavoriteList(LifecycleOwner owner){
        LiveData<List<FavoriteProductsData>> liveData = favoriteDao.getAllFavoriteProducts();
        liveData.observe(owner, favoriteProductsData -> {
            fragmentView.getFavoriteList(favoriteProductsData);
        });
    }
}
