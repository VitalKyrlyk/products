package foxminded.products.Fragments.SearchFragment.rvAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import foxminded.products.Activity.DetailedActivity.DetailedActivity;
import foxminded.products.R;
import foxminded.products.Retrofit.ModelLists.ProductList;
import foxminded.products.utils.OnNextPageListener;

public class SearchFragmentAdapter extends RecyclerView.Adapter<SearchFragmentAdapter.ViewHolder> {

    private List<ProductList.ProductItem> productItems;
    private Context context;

    private final int visibleThreshold = 3;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int totalItemCount;
    private int lastVisibleItemPosition;
    private int firstVisibleItem;
    private boolean isReverseLayout;

    public SearchFragmentAdapter(Context context, RecyclerView recyclerView,
                                 LinearLayoutManager mLayoutManager, OnNextPageListener listener) {
        this.context = context;
        isReverseLayout = mLayoutManager.getReverseLayout();
        recyclerView.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                firstVisibleItem = mLayoutManager.findLastCompletelyVisibleItemPosition();
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();
                if (loading && (totalItemCount > previousTotalItemCount)) {
                    loading = false;
                    previousTotalItemCount = totalItemCount;
                }
                if (!loading && needLoading()) {
                    loading = listener.onLoadMore();
                }
            }
        });
    }

    private boolean needLoading() {
        if (isReverseLayout)
            return totalItemCount - firstVisibleItem <= visibleThreshold;
        else
            return (visibleThreshold + firstVisibleItem) >= totalItemCount;
    }

    public void setData(List<ProductList.ProductItem> data){
        this.productItems = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvTitle.setText(productItems.get(position).getTitle());
        String image = productItems.get(position).getMainImage().getUrl_170x135();
        if (image != null){
            Glide.with(context)
                    .load(image)
                    .into(holder.ivIcon);
        }

        if (productItems != null) {
            final String title = productItems.get(position).getTitle();
            final String description = productItems.get(position).getDescription();
            final String perice = "Price: " + productItems.get(position).getPrice() + " " +
                    productItems.get(position).getCurrency_code();
            final String listing_id = productItems.get(position).getListing_id();

            holder.view.setOnClickListener(view -> {
                ArrayList<String> list = new ArrayList<>();
                list.add(0, title);
                list.add(1, description);
                list.add(2, perice);
                list.add(3, image);
                list.add(4, listing_id);

                Intent intent = new Intent(context, DetailedActivity.class);
                intent.putStringArrayListExtra("list", list);
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (productItems == null){
            return 0;
        } else {
            return productItems.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle;
        ImageView ivIcon;
        View view;

       ViewHolder(View itemView) {
           super(itemView);
           this.view = itemView;
           tvTitle = itemView.findViewById(R.id.tvTitleSearch);
           ivIcon = itemView.findViewById(R.id.ivIconSearch);
       }
   }
}
