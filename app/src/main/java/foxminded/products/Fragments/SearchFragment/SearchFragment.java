package foxminded.products.Fragments.SearchFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;
import foxminded.products.Fragments.SearchFragment.Presenter.SearchFragmentPresenter;
import foxminded.products.Fragments.SearchFragment.View.SearchFragmentView;
import foxminded.products.Fragments.SearchFragment.rvAdapter.SearchFragmentAdapter;
import foxminded.products.R;
import foxminded.products.Retrofit.ApiInterface;
import foxminded.products.Retrofit.ModelLists.CategoryList;
import foxminded.products.Retrofit.ModelLists.ProductList;

public class SearchFragment extends DaggerFragment implements SearchFragmentView,
        SwipeRefreshLayout.OnRefreshListener {

    View view;
    SearchFragmentAdapter adapter;
    SearchFragmentPresenter presenter;

    String category_id;
    String categoryName;
    String keywords;

    private int mPage = 1;
    private int limit = 25;

    @Inject
    ApiInterface apiInterface;

    @BindView(R.id.rvSearchList)
    RecyclerView rvSearch;

    @BindView(R.id.spinnerCategory)
    Spinner spinner;

    @BindView(R.id.etSearch)
    EditText etSearch;

    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    @OnClick(R.id.btnSubmit)
    void btnSubmit(){
        if (etSearch.getText().length() == 0){
            etSearch.setError("This field must not be empty!");
        } else {
            mPage = 1;
            swipeContainer.setRefreshing(true);
            keywords = etSearch.getText().toString();
            presenter.loadData(apiInterface, mPage, limit, categoryName, keywords);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, null);
        ButterKnife.bind(this, view);

        presenter = SearchFragmentPresenter.getInstance();
        presenter.getView(this);
        presenter.getCategoryList(apiInterface);

        swipeContainer.setOnRefreshListener(this);
        initRecyclerView();

        return view;
    }

    @Override
    public void setCategoryList(final List<CategoryList.CategoryItems> categoryList) {

        final List<String> items = new ArrayList<>();
                for (int i = 0; i<categoryList.size(); i++) {
                   String name = categoryList.get(i).getName();
                   items.add(name);
                }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
                R.layout.simple_spinner_item, items);
        spinner.setAdapter(arrayAdapter);
        spinner.setPrompt(getString(R.string.category));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                categoryName = categoryList.get(i).getName();
                category_id = categoryList.get(i).getCategory_id();
                Toast.makeText(getContext(), categoryName , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadMore() {
        presenter.loadData(apiInterface, mPage, limit, categoryName, keywords);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        adapter = new SearchFragmentAdapter(getContext(), rvSearch, layoutManager, () ->{
            loadMore();
            return true;
        });
        rvSearch.setLayoutManager(layoutManager);
        rvSearch.setAdapter(adapter);
    }

    @Override
    public void setProductList(List<ProductList.ProductItem> productList, int mPage) {
        this.mPage = mPage;
        limit = limit + 25;
        adapter.setData(productList);
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void showIsSuccessfulError() {
        Toast.makeText(getContext(), R.string.isSuccessfulError, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showConnectionError() {
        Toast.makeText(getContext(), R.string.connectionError, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyResponseBodyError() {
        Toast.makeText(getContext(), R.string.emptyResponseBodyError, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        if (etSearch.getText().length() == 0) {
            etSearch.setError("This field must not be empty!");
            swipeContainer.setRefreshing(false);
        } else {
            limit = 25;
            mPage = 1;
            presenter.loadData(apiInterface, mPage, limit, categoryName, keywords);
        }
    }
}
