package foxminded.products.Fragments.SearchFragment.Presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.Objects;

import foxminded.products.Fragments.SearchFragment.View.SearchFragmentView;
import foxminded.products.Retrofit.ApiInterface;
import foxminded.products.Retrofit.ModelLists.CategoryList;
import foxminded.products.Retrofit.ModelLists.ProductList;
import foxminded.products.Retrofit.NetworkContract;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragmentPresenter {

    private static SearchFragmentPresenter instance;
    private SearchFragmentView fragmentView;

    private SearchFragmentPresenter() {
    }

    public static synchronized SearchFragmentPresenter getInstance() {
        if (instance == null) {
            instance = new SearchFragmentPresenter();
        }
        return instance;
    }

    public void getView(SearchFragmentView fragmentView) {
        this.fragmentView = fragmentView;
    }

    public void loadData(ApiInterface apiInterface, int mPage, int limit, String category, String keywords) {
        Call<ProductList> productListCall = apiInterface.getProductsList(mPage, limit, category, keywords);
        Log.d("myTag", "PAGE " + mPage);
        productListCall.enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(@NonNull Call<ProductList> call, @NonNull Response<ProductList> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (Objects.requireNonNull(response.body()).getProductItems() == null) {
                            fragmentView.showEmptyResponseBodyError();
                            Log.d("myTag", "Products ERROR: " + response);
                        } else {
                            List<ProductList.ProductItem> productItems = Objects.requireNonNull(response.body()).getProductItems();
                            int page = Objects.requireNonNull(response.body()).getPagination().getNext_page();
                            fragmentView.setProductList(productItems, page);
                            Log.d("myTag", "Products OK: " + response);
                        }
                    } else {
                        fragmentView.showIsSuccessfulError();
                        Log.d("myTag", "Products ERROR: " + response);
                    }
                } else {
                    fragmentView.showIsSuccessfulError();
                    Log.d("myTag", "Products ERROR: " + response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductList> call, @NonNull Throwable t) {
                fragmentView.showConnectionError();
                Log.d("myTag", "Product ERROR: " + t);
            }
        });
    }

    public void getCategoryList(ApiInterface apiInterface) {

        Call<CategoryList> categoryListCall = apiInterface.getCategoryData(NetworkContract.Etsy.API_KEY);
        categoryListCall.enqueue(new Callback<CategoryList>() {
            @Override
            public void onResponse(@NonNull Call<CategoryList> call, @NonNull Response<CategoryList> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (Objects.requireNonNull(response.body()).getCategoryItems() == null) {
                            fragmentView.showEmptyResponseBodyError();
                            Log.d("myTag", "Category ERROR: " + response);
                        } else {
                            List<CategoryList.CategoryItems> categoryItems = Objects.requireNonNull(response.body()).getCategoryItems();
                            fragmentView.setCategoryList(categoryItems);
                            Log.d("myTag", "Category OK: " + response);
                        }
                    } else {
                        fragmentView.showIsSuccessfulError();
                        Log.d("myTag", "Category OK: " + response);
                    }
                } else {
                    fragmentView.showIsSuccessfulError();
                    Log.d("myTag", "Category OK: " + response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryList> call, @NonNull Throwable t) {
                fragmentView.showConnectionError();
                Log.d("myTag", "Category ERROR: " + t);
            }
        });

    }

}
