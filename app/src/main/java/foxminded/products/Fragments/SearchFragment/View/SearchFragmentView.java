package foxminded.products.Fragments.SearchFragment.View;

import java.util.List;

import foxminded.products.Retrofit.ModelLists.CategoryList;
import foxminded.products.Retrofit.ModelLists.ProductList;

public interface SearchFragmentView {

    void setCategoryList(List<CategoryList.CategoryItems> categoryList);

    void setProductList(List<ProductList.ProductItem> productList, int page);

    void showIsSuccessfulError();

    void showConnectionError();

    void showEmptyResponseBodyError();
    
}
