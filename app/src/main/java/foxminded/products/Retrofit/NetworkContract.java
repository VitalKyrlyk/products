package foxminded.products.Retrofit;

public class NetworkContract {

    public static final class Etsy {

        public static final String API_KEY = "013ncqxq3ruair0dllpzlvc9";

        private Etsy() {
        }
    }

    public static final class ProductParam {

        private static final String LISTING_ID = "listing_id";
        private static final String TITLE = "title";
        private static final String PRICE = "price";
        private static final String CURRENCY_CODE = "currency_code";
        private static final String DESCRIPTION = "description";
        public static final String PRODUCT_FIELDS = LISTING_ID + "," + TITLE + "," + DESCRIPTION + "," + PRICE + "," + CURRENCY_CODE;

        private static final String MAIN_IMAGE = "MainImage";
        private static final String THUMBNAIL_SIZE = "url_170x135";
        private static final String FULL_SIZE = "url_570xN";
        public static final String IMAGE_INCLUDE = MAIN_IMAGE + "(" + THUMBNAIL_SIZE + "," + FULL_SIZE + ")";

        private ProductParam() {
        }
    }

}
