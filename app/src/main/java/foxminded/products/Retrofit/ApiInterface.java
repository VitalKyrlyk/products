package foxminded.products.Retrofit;

import foxminded.products.Retrofit.ModelLists.CategoryList;
import foxminded.products.Retrofit.ModelLists.ProductList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

    public interface ApiInterface {

        @GET("taxonomy/categories?")
        Call<CategoryList> getCategoryData(@Query("api_key") String key);

        @GET("listings/active?" +

                "api_key=" + NetworkContract.Etsy.API_KEY +

                "&fields=" + NetworkContract.ProductParam.PRODUCT_FIELDS +

                "&includes=" + NetworkContract.ProductParam.IMAGE_INCLUDE
        )

        Call<ProductList> getProductsList(@Query("page") int page,
                                          @Query("limit") int limit,
                                          @Query("category") String category,
                                          @Query("keywords") String keywords);
    }
