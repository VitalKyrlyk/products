package foxminded.products.Retrofit.ModelLists;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryList {

    public class CategoryItems {

        String name;

        String category_id;

        public String getName() {
            return name;
        }

        public String getCategory_id() {
            return category_id;
        }
    }

    @SerializedName("results")
    private List<CategoryItems> results;

    public List<CategoryItems> getCategoryItems() {
        return results;
    }
}
