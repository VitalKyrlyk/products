package foxminded.products.Retrofit.ModelLists;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductList {

    public class MainImage {

            private String url_75x75;

            private String url_170x135;

            private String url_fullxfull;

            public String getUrl_75x75() {
                return url_75x75;
            }

            public String getUrl_170x135() {
                return url_170x135;
            }

            public String getUrl_fullxfull() {
                return url_fullxfull;
            }
    }

    public class Pagination{

        int next_page;

        public int getNext_page() {
            return next_page;
        }
    }

    public class ProductItem {

        String title;

        String description;

        String price;

        String currency_code;

        String listing_id;

        @SerializedName("MainImage")
        MainImage mainImage;

        public MainImage getMainImage() {
            return mainImage;
        }

        public String getListing_id() {
            return listing_id;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getPrice() {
            return price;
        }

        public String getCurrency_code() {
            return currency_code;
        }

    }

    @SerializedName("results")
    private List<ProductList.ProductItem> results;

    public List<ProductList.ProductItem> getProductItems() {
        return results;
    }

    @SerializedName("pagination")
    private Pagination pagination;

    public Pagination getPagination() {
        return pagination;
    }

}
